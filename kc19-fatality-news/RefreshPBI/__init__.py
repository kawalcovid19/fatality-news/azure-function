import logging
import requests

import azure.functions as func
import os

PBIE_GROUP_ID = os.environ['PBIE_GROUP_ID']
PBIE_REPORT_ID = os.environ['PBIE_REPORT_ID']
PBIE_DATASET_ID = os.environ['PBIE_DATASET_ID']


class PBIWorkspace:
    def __init__(self, group_id, settings=None):
        self.username = os.environ["PBIE_USERNAME"]
        self.password = os.environ["PBIE_ACC_PASSWORD"]
        self.client_id = os.environ["PBIE_CLIENT_ID"]
        self.group_id = group_id
        self.access_token = self.get_access_token()

    def get_access_token(self):
        data = {
            'grant_type': 'password',
            'scope': 'openid',
            'resource': r'https://analysis.windows.net/powerbi/api',
            'client_id': self.client_id,
            'username': self.username,
            'password': self.password
        }
        response = requests.post('https://login.microsoftonline.com/common/oauth2/token', data=data)
        return response.json().get('access_token')

    def update_dateset(self, dataset_id):
        access_token = self.access_token
        refresh_url = 'https://api.powerbi.com/v1.0/myorg/groups/{}/datasets/{}/refreshes'.format(self.group_id,
                                                                                                  dataset_id)
        header = {'Authorization': f'Bearer {access_token}'}
        r = requests.post(url=refresh_url, headers=header)
        r.raise_for_status()
        return r

def main(mytimer: func.TimerRequest) -> None:
    PBIWorkspace(PBIE_GROUP_ID).update_dateset(PBIE_DATASET_ID)
