import os

import pandas as pd

import azure.functions as func
from azure.storage.blob import BlockBlobService

from .helper import get_patient_status, get_provinsi


def main(mytimer):
    df = pd.read_csv(
        "https://docs.google.com/spreadsheets/u/1/d/1RiLo8ZvOSurwBkEqOo_BcS1h_TFmE-052p9pxqTbF94/export?format=csv"
    )
    df = df.iloc[1:].reset_index(drop=True)

    df["#"] = df.index + 1

    df["patient_status"] = df["Patient status"].apply(get_patient_status)

    df["provinsi"] = df["Location of demise"].apply(lambda x: get_provinsi(x).upper())
    
    # Upload to Azure Storage (Full Data)
    accountName = "kawalcovid19"
    accountKey = os.environ["AZURE_BLOB_KAWALCOVID19_KEY"]
    containerName = os.environ["AZURE_BLOB_CONTAINER_NAME"]

    blobService = BlockBlobService(account_name=accountName, account_key=accountKey)
    blobService.create_blob_from_text(
        containerName,
        "data/raw/fatality_news.csv",
        df.to_csv(index=False, encoding="utf-8"),
    )
