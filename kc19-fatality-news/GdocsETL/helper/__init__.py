from .data_source import provinces, rev_idx_kab_or_kota


def edit_dist(str1, str2, m, n):
    dp = [[0 for x in range(n + 1)] for x in range(m + 1)]

    for i in range(m + 1):
        for j in range(n + 1):
            if i == 0:
                dp[i][j] = j
            elif j == 0:
                dp[i][j] = i
            elif str1[i - 1] == str2[j - 1]:
                dp[i][j] = dp[i - 1][j - 1]
            else:
                dp[i][j] = 1 + min(dp[i][j - 1], dp[i - 1][j], dp[i - 1][j - 1])
    return dp[m][n]


def get_patient_status(x):
    if type(x) != str:
        return "?"
    lower_x = x.lower().strip()
    if "pdp" == lower_x:
        return "PDP"
    elif "odp" == lower_x:
        return "ODP"
    elif "confirmed" in lower_x or "positive" in lower_x or "aktif" in lower_x:
        return "AKTIF"
    else:
        return "tidak diketahui"


def get_provinsi(x):
    if type(x) != str:
        return "?"
    _min = 999999
    val = None
    for v in provinces:
        ed = edit_dist(x, v, len(x), len(v))
        if ed < _min:
            _min = ed
            val = v
    if _min == 0:
        return val
    _min2 = 99999
    val2 = None
    for v in rev_idx_kab_or_kota:
        ed = edit_dist(x, v, len(x), len(v))
        if ed < _min2:
            _min2 = ed
            val2 = rev_idx_kab_or_kota[v]
    if _min2 == 0 or _min2 < _min:
        return val2
    return val
