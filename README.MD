# Azure Function Repository for Fatality-Analytics

## How to Deploy Azure function
First make sure to setup azure-cli in your local computer : https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest

1. Go to function directory (i.e. `kc19-fatality-news`)
2. Run following command : `func azure functionapp publish <function-app-name>`

